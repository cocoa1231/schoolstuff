#include <iostream>
using namespace std;

struct input {
    float force;
    float linear;
    float quad;
    float cube;
    float constant;
    float distance;
};

float linear(float coef, float constant, float distance) {
    int i;
    float n = 1000;
    float delta_x = distance/n;
    float sum = 0;
    float x = delta_x;
    for (i = 0; i < 1000; ++i) {
        sum = sum + (coef*(x) + constant) * delta_x;
        x = x + delta_x;
    }
    return sum;
}

float quadratic(float sq_coef, float li_coef, float constant, float distance) {
    int i;
    float n = 1000;
    float delta_x = distance/n;
    float sum = 0;
    float x = delta_x;
    for (i = 0; i < n; ++i) {
        sum = sum + (sq_coef*(x*x) + li_coef*(x) + constant) * delta_x;
        x = x + delta_x;
    }
    return sum;
}

float cubic(float cub_coef, float sq_coef, float li_coef, float constant, float distance) {
    int i;
    float n = 1000;
    float delta_x = distance/n;
    float sum = 0;
    float x = delta_x;
    for (i = 0; i < n; ++i) {
        sum = sum + (cub_coef*(x*x*x) + sq_coef*(x*x) + li_coef*(x) + constant) * delta_x;
        x = x + delta_x;
    }
    return sum;
}

int main(int argc, char *argv[])
{
    int exit = 0;
    input inst;
    do {

            std::cout << "Hello! Would you like to calculate the work done for: " << std::endl;
            std::cout << "1. A constant force over some distance" << std::endl;
            std::cout << "2. A variable force as a function of position over some distance" << std::endl;
            std::cout << "Your choice: ";
            int choice;
            cin.clear();
            fflush(stdin);
            cin >> choice;
            if (choice == 1) {
                cin.clear(); fflush(stdin);
                std::cout << "Please enter the force in Newtons: "; cin >> inst.force;
                std::cout << "Enter the distance in meters: "; cin >> inst.distance;;
                std::cout << "The work done was: " << inst.force*inst.distance << std::endl;
            }
            else if (choice == 2) {
                cin.clear(); fflush(stdin);
                std::cout << "Is the function linear, quadratic or cubic?" << std::endl;
                std::cout << "1 = Linear, 2 = Quadratic, 3 = Cubic" << std::endl;
                int functype;
                cin >> functype;
                switch (functype) {
                    case 1:
                    {
                        cin.clear(); fflush(stdin);
                        std::cout << "Okay, enter the coefficient of x: "; cin >> inst.linear;
                        std::cout << "Now the constant: "; cin >> inst.constant;
                        std::cout << "Finally, enter the distance over which it travelled: "; cin >> inst.distance;
                        std::cout << "Result: " << linear(inst.linear, inst.constant, inst.distance) << std::endl;
                        break;
                    }
                    case 2:
                    {
                        cin.clear(); fflush(stdin);
                        std::cout << "Enter the coefficient of x^2: "; cin >> inst.quad;
                        std::cout << "Enter the coefficient of x:   "; cin >> inst.linear;
                        std::cout << "Enter the constant: "; cin >> inst.constant;
                        std::cout << "Enter the distance travelled: "; cin >> inst.distance;
                        std::cout << "Result: " << quadratic(inst.quad, inst.linear, inst.constant, inst.distance) << std::endl;
                        break;
                    }
                    case 3:
                    {
                        cin.clear(); fflush(stdin);
                        std::cout << "Enter the coefficient of x^3: "; cin >> inst.cube;
                        std::cout << "Enter the coefficient of x^2: "; cin >> inst.quad;
                        std::cout << "Enter the coefficient of x: "; cin >> inst.linear;
                        std::cout << "Enter the constant: "; cin >> inst.constant;
                        std::cout << "Enter the distance: "; cin >> inst.distance;
                        std::cout << "Result: " << cubic(inst.cube, inst.quad, inst.linear, inst.constant, inst.distance) << std::endl;
                        break;
                    }
                }
            }

            std::cout << "Would you like to do it again? [Y/n] ";
            char answer;
            cin.clear(); fflush(stdin);
            cin >> answer;
            if (answer=='Y') {
                exit = 0;
                std::cout << "\n\n\n";
            }
            else if (answer = 'n') {
                exit = 1;
            }
            else {
                exit = 0;
            }
            
        } while (exit == 0);
        return 0;
}
