#include <iostream>
#include <string>

/*
 *  A heavily commented implementaion of a basic LIFO stack
 *  Sorry...
 */


/*
 * Explanation of a LIFO stack:
 * LIFO stands for last in first out. Let's imagine you are going
 * to put some items on a literal physical stack. So all of the stuff
 * is one on top of another like so:
 *
 *   [ 4 ]
 *   [ 3 ]
 *   [ 2 ]
 *   [ 1 ]
 *
 * Each square bracket represents an item. If those were books, you'd
 * be stacking books here. Now the idea is the book on the top of the stack
 * (4) would be picked up first, and then third, then second and then first.
 * The book that was the last one to be piled up on the stack is the first thing
 * you'd pick up.
 *
 * Now in a computer to implement this, you're going to use some data structure
 * (a struct) that has a pointer to the previous and next items on the stack. Think
 * of it like this. The first page on book 3 tells you that the thing below it is 
 * book two, and a the end it tells you the thing on the top is book four. In the middle 
 * are the contents of the book. If we put all those books in a line:
 *
 * [ 1 ] <-> [ 2 ] <-> [ 3 ] <-> [ 4 ]
 *
 * Those (<->) represent links. At this point, I'm going to drop the analogy. All of those
 * books are the node data structers we'll declair, the front and back are pointers to the 
 * next and previous items in the stack.
 *
 * Pushing to the stack:
 *
 * If we were gonna pile on another book, we'll write on the back of the fourth book the title
 * of the new book. In computer-terms, we'll set the next pointer at the end of the stack to point
 * to the new node.
 *
 * [ 1 ] <-> [ 2 ] <-> [ 3 ] <-> [ 4 ] <-> [ 5 ]
 *
 * Popping from the stack:
 *
 * To remove from the stack, we'll just tell the fourth book that there is nothing next to it
 * by setting the next pointer to, well, NULL.
 *
 * [ 1 ] <-> [ 2 ] <-> [ 3 ] <-> [ 4 ]
 *
 * The need for an entry point:
 *
 * Well the last thing we need is a pointer to one of the objects in the stack. We need some
 * point of entry. Normally it's the first item on the stack, or the last (often called Top Of
 * Stack, or TOS). So our stack implementaion will have an entry point pointer.
 *
 * With that, let's get to the code!
 *
 */

struct node { // Our node item
    struct node* previous;
    std::string item;
    struct node* next;
};

class CustomStack { // The actual implementation of the stack
public:
    CustomStack(std::string first) {
        // ctor:
        // Initialize empty node with first node item
        
        // Let the previous and next pointers be NULL
        // We have nothing on the stack apart from the
        // first item
        
        struct node* temporary = new struct node;

        temporary->previous = NULL;
        temporary->next     = NULL;
        temporary->item     = first;

        //// We have one item. The top and bottom are the same thing
        this->BOS = temporary;
        this->TOS = temporary;
    }

    // typedef ssize_t unsigned int
    // ssize_t is an unsigned integer defined in <iostream>
    ssize_t push(std::string push_me) {
        /*
         * Pushing a node onto the stack
         */

        struct node NewNode;

        NewNode.previous = this->TOS;
        NewNode.previous->next = &NewNode; 
        NewNode.item = push_me;
        NewNode.next = NULL;
        
        // Finally, the top of stack needs to be the new pushed item
        this->TOS = &NewNode; 

        return 0;
    }

    ssize_t pop_top() {
        this->TOS->previous->next = NULL; // Get the item next to the top and set the top to NULL
        this->TOS = this->TOS->previous;  // TOS is the previous node
        return 0;
    }

    void print_stack() {
        struct node* current_node = this->BOS;
        std::cout << "[";
        while ( current_node != NULL ) {
            std::cout << current_node->item << ", ";
            current_node = current_node->next;
        }
        std::cout << ']';
    }
private:
    struct node* TOS;
    struct node* BOS; // Bottom Of Stack. This is my entry point
};

int main(int argc, char *argv[]) {
    std::string init = "first";
    CustomStack stk(init);

    stk.print_stack();
    std::string ITEM = "second";

    stk.push(ITEM);
    stk.print_stack();
    return 0;
}
