#include <iostream>

int lsearch(int array[], int element, int size) {

    int i;
    for (i = 0; i < size; ++i) {
        if (array[i] == element) {
            return i;
        }
    }
    return -1;
}

int main(int argc, char *argv[])
{
    int array[] { 1,2,3,4,5 };
    int i = lsearch(array, 2, 5);
    if (i) {
        std::cout << "Found at " << i << std::endl;
        return 0;
    }
    std::cout << "Element not found" << std::endl;
    return 0;
}
