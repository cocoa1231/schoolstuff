#include <iostream>
#include <cstdlib>
using namespace std; // #include <conio.h> for TurboC++

int main(int argc, char* argv[]) {
	float largest = 0.0;
	if (argc == 11) {
		for (int i = 0; i < argc; ++i) {
			if (atof(argv[i]) > largest) {
				largest = atof(argv[i]);
			}
		}
	}
	else {
		cout << "I only accept 10 numbers" << endl;
		return -1;
	}
	cout << "Largest number: " << largest << endl;
	return 0;
}
