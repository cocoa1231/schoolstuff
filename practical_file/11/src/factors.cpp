#include <iostream>
#include <cstdlib>
#include <vector>

int main(int argc, char* argv[]) {
	int number = atoi(argv[1]);
	int i;
	std::cout << "Factoring " <<  number << std::endl;
	std::vector<int> factors;
	for (i = 2; i < number; ++i) {
		if ((number % i) == 0) {
			factors.push_back(i);
		}
	}

	std::cout << "Factors are: ";
	for (i = 0; i < factors.size(); ++i) {
		if (i != factors.size()-1) {
			std::cout << factors[i] << ", ";
		}
		else {
			std::cout << factors[i];
		}
	}

	std::cout << "\nNumber of factors: " << factors.size() << std::endl;

	return 0;
}
