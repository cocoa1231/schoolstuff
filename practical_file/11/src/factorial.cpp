#include <iostream>
#include <cstdlib>
using namespace std;

int fact(int n) {
	if (n == 0 || n == 1) {
		return 1;
	}
	return n*fact(n-1);
}

int main(int argc, char* argv[]) {
	int number = atoi(argv[1]);
	
	cout << "Factorial of "<< number << " is " << fact(number) << endl;

	return 0;
}
