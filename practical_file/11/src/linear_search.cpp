#include <iostream>

/* Jatin Kaushal
 * XII - A
 * Roll Number: 17
 */

int lsearch(int array[], int element, int array_size) {
    int i;
    for (i = 0; i < array_size; ++i) {
        if (array[i] == element)
           return i+1;
    }
    return -1;
}

int main(int argc, char *argv[])
{
    int array[] { 5,2,4,5,1,2,3 };
    int result = lsearch(array, 3, sizeof(array)/sizeof(array[0]));
    if (result)
        std::cout << "Element found at " << result << std::endl;
    else
        std::cout << "Element not found" << std::endl;
    return 0;
}
