#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;

int main(int argc, char *argv[])
{
        int i;
        for (i = 0; i < argc; ++i) {
                int s = atoi(argv[i]);
                if (i != 0) {
                        if (i % 2 == 0) {
                                cout << atof(argv[i])*2 << " ";
                        }
                        else {
                                cout << atof(argv[i])/2 << " ";
                        }
                }
        }
        cout << endl;
        return 0;
}
