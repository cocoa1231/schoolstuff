#include <iostream>
#include <cctype>
#include <string>
#include <cstdlib>
using namespace std;

int main(int argc, char* argv[]) {
        int i,j,k, len;
        char* target, c;
        if (argc >= 5) {
                for (i = 0; i < argc; ++i) {
                        string arg = argv[i];
                        if (arg == "-s" || arg == "--string") { 
                                target = argv[i+1];
                        }
                        if (arg == "-l" || arg == "--length") { 
                                len = atoi(argv[i+1]);
                        }
                }
                int index = 0;
                for (i = 0; i < len; ++i) {
                        target[i] = tolower(target[i]);
                }
                cout << target << endl;
        }
        else { 
                cout << "Usage: toLower {-s|--string} STRING {-l|--length} LENGTH";
        }
        return 0;
}
