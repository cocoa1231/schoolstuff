#include <iostream>
using namespace std;

int main() {
    char str[] = "This is a string"; int count = 1;
    for (int i = 0; i < sizeof(str); ++i) {
        if (str[i] == ' ') {
            count++;
        }
    }
    std::cout << "Total words: " << count << std::endl;
}
