#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;

void reverse(char* s, int size) {
        for(int i = size; i >= 0; i--) {
                cout << s[i];
        }
}

int main(int argc, char *argv[])
{
        int i, len;
        char* target;
        if(argc >= 5) {
                for (i = 0; i < argc; ++i) {
                        string arg = argv[i];
                        if (arg == "-s" || arg == "--string") {
                                target = argv[i+1];
                        }
                        if (arg == "-l" || arg == "--length") {
                                len = atoi(argv[i+1]);
                        }
                }
                reverse(target, len);
                return 0;
        }
        else {
                cout << "Usage: revString -s STRING -l LEN" << endl;
                return 0;
        }
}
