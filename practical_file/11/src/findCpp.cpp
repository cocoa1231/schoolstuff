#include <iostream>
using namespace std;

int main() {
	int findInt;
	cout << "Enter the integer you want to find: ";
	cin >> findInt;
	int array[] = { 1,2,3,4,5,6,7,8,9,10 };
	for (int i = 0; i < 10; i++) {
		if (array[i] == findInt) {
			cout << "\nFound at index " << i << endl;
			return 0;
		}

	}
	cout << "Not found" <<endl;

	return -1;
}
