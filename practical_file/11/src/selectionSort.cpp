#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
        int i, j;
        int array[] = { 5,4,3,2,1 };
        for (i = 0; i < 5; ++i) {
            for (j = 0; j < 5; j++) {
                if (array[i] < array[j]) {
                    int temp = array[j];
                    array[j] = array[i];
                    array[i] = temp;
                }
            }
        }
        for (i = 0; i < 5; ++i) {
                std::cout << array[i] << std::endl;
        }
        return 0;
}
