#include <iostream>
using namespace std;

void swap(int *first, int *second) {
    int temp = *first;
    *first = *second;
    *second = temp;
}

void bubbleSort(int arr[], int size) {
    int i, j;
    for (i = 0; i < size; ++i) {
        for (j = 0; j < size-i; ++j) {
            if (arr[j] > arr[j+1]) {
                swap(&arr[j], &arr[j+1]);
            }
        }
    }
}

int main() {
    int i;

    int array[] = { 5,4,3,2,1 };
    bubbleSort(array, 5);
    for (i = 0; i < 5; i++) {
        std::cout << array[i] << std::endl;
    }
    return 0;
}
