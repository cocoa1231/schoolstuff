#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

int main(int argc, char* argv[]) {
        int col;
        if (argc >= 3) {
                for (int i = 0; i < argc; i++) {
                        string arg = argv[i];
                        if (arg == "-c" || arg == "--column") {
                                col = atoi(argv[i+1]);
                        }
                }

                int matrix[][3] = { {1,2,3},
                                    {4,5,6},
                                    {7,8,9} };

                int i, j;
                int sum = 0;
                for (i = 0; i < 3; i++) {
                        sum = sum + matrix[i][col%3];
                }
                cout << "Sum is: " << sum << endl;
        }
        else {
                cout << "Usage: sumCols -c COL" << endl;
        }
        return 0;
}
