#include <iostream>
#include <cstdlib>
#include <string>
#include <cctype>
using namespace std;

int main(int argc, char *argv[])
{
        char* target;
        int i,length;
        if (argc >= 5) {
                for (i = 0; i < argc; ++i) {
                        string arg = argv[i];
                        if (arg == "-s" || arg == "--string") {
                                target = argv[i+1];
                        }
                        if (arg == "-l" || arg == "--length") {
                                length = atoi(argv[i+1]);
                        }
                }
                int vowel = 0;
                for (i = 0; i < length; ++i) {
                        char test = target[i];
                        switch (target[i]) {
                            case 'a':
                                vowel++;
                                break;
                            case 'e':
                                vowel++;
                                break;
                            case 'i':
                                vowel++;
                                break;
                            case 'o':
                                vowel++;
                                break;
                            case 'u':
                                vowel++;
                                break;
                        }
                }
                std::cout << "Total count was: " << vowel << std::endl;
                
        }
        else {
                std::cout << "Usage: vowelCount -s [lower case string] -l [length of string]" << std::endl;
        }
        return 0;
}
