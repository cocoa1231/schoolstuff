#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;


int main(int argc, char *argv[])
{
        char* str;
        int i, length;
        bool flag;
        if (argc >= 5) {
                for (i = 0; i < argc; ++i) {
                        string arg = argv[i];
                        if (arg == "-s" || arg == "--string") {
                                str = argv[i+1];
                        }
                        if (arg == "-l" || arg == "--length") {
                                length = atoi(argv[i+1]);
                        }
                }
                int breaker = 0;
                for (i = 0; i < length; ++i) {
                        if (str[i] != str[length-i-1]) {
                                breaker = 1;
                                break;
                        }
                }

                if (breaker) {
                        std::cout << "String is not a pallendrome" << std::endl;
                }
                else {
                        std::cout << str << " is a pallendrome"  << std::endl;
                }
        }
        return 0;
}
