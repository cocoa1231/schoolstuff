#include <iostream>
using namespace std;

int main() {
    int i, j;
    int arr[][3] = { {1,2,3}, {4,5,6}, {7,8,9} };
    int sum = 0;
    for (i = 0; i < 3; i++) {
        for (j = 2; j >= 0; j--) {
            sum = sum + arr[i][j];
        }
    }
    std::cout << sum << std::endl;

}
