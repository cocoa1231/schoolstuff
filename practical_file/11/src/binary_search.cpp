#include <iostream>

/* Jatin Kaushal
 * XII - A
 * Roll Number: 17
 */

int binary_search(int array[], int element, int size) {
    int lower = 0, upper = size, mid;
    while (lower <= upper) {
        mid = (upper+lower)/2;
        if (array[mid] == element)
            return mid+1;
        else if (array[mid] > element)
            upper = mid-1;
        else
            lower = mid+1;
    }
    return -1;
}

int main(int argc, char *argv[])
{
    int array[] { 1, 4, 5, 6, 7, 10, 24, 155 };
    int element = 155;
    int size = sizeof(array)/sizeof(array[0]);
    int result = binary_search(array, element, size);

    if (result)
        std::cout << "Element found at " << result << std::endl;
    else
        std::cout << "element not found" << std::endl;

    return 0;
}
