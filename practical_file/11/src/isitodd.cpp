#include <iostream>
#include <cstdlib>
#include <string>
using namespace std; // if TurboC++ #include <conio.h>

int main(int argc, char *argv[])
{
		int i, j, k, num;
		if (argc >= 3) {
				for (i = 0; i < argc; ++i) {
						string arg = argv[i];
						if (arg == "-n" || arg == "--number") {
								num = atoi(argv[i+1]);
						}
				}
				if (num % 2 == 0) {
						std::cout << "It is not odd" << std::endl;
						return 0;
				}
				else {
						std::cout << "That's odd" << std::endl;
						return 0;
				}
		}
		else {
			std::cout << "Usage: isitodd [-n|--number] NUMBER" << std::endl;
		}
		return 0;
}
