#include <iostream>
#include <cstdlib>
#include <string>
using namespace std; // #include <conio.h> for TurboC++

int main(int argc, char *argv[]) {
	int age;
	if (argc >= 3) {
		for (int i = 0; i < argc; ++i) {
			string arg = argv[i];
			if(arg == "-a" || arg == "--age") {
				age = atof(argv[i+1]);
			}
		}
	}
	else {
		cout << "Please enter age by doing --age <age>";
		return -1;
	}
	cout << "Age: " << age << endl;
	if (age >= 18) {
		cout << "Yes you can" << endl;
	}
	else {
		cout << "You shall not pass" << endl;
	}
	return 0;
	
}
