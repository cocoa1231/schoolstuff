#include <iostream>

void concat(char* string1, char* string2, char* output, int sizes[]) {
    int i, sum = sizes[0] + sizes[1];
    for (i = 0; i < sum; ++i) {
        if (i - sizes[0]) {
            output[i] = string1[i];
        }
        else {
            output[i] = string2[i];
        }
    }
}

int main(int argc, char *argv[])
{
    char str1[] = "This is ";
    char str2[] = "a string";
    char* str;
    int sizes[] { sizeof(str1)/sizeof(str1[0]), sizeof(str2)/sizeof(str2[0]) };
    concat(str1, str2, str, sizes);
    std::cout << str << std::endl;
    return 0;
}
