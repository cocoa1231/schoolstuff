#include <iostream>
#include <cstdlib>
using namespace std; // #include <conio.h> for TurboC++

int main(int argc, char* argv[]) {
	float largest = 0.0;
	for (int i = 0; i < argc; ++i) {
		if (atof(argv[i]) > largest) {
			largest = atof(argv[i]);
		}
	}
	cout << "Largest number: " << largest << endl;
	return 0;
}
