#include <iostream>
#include <string>
#include <cstdlib>
using namespace std; // If TruboC++, #include <conio.h>

int main(int argc, char *argv[])
{
		int i, j, k, limit;
		if (argc >= 3) {
				for (i = 0; i < argc; ++i) {
						string arg = argv[i];
						if (arg == "-l" || arg == "--limit") {
								limit = atoi(argv[i+1]);
						}
				}
		}
		else {
				std::cout << "Usage: printevensquare [-l|--limit] NUMBER" << std::endl;
				return 0;
		}
		int count = 1;
		std::cout << "Limit: " << limit << std::endl;
		while(count<=limit) {
				if(count%2==0) {
						if(count==limit) {
								std::cout << count*count << std::endl;
						}
						else {
								std::cout << count*count << ", ";

						}
				
				}
				count++;
		}
		return 0;
}
