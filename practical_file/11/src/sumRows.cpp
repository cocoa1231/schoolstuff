#include <iostream>
using namespace std;

int main() {
    int arr[][3] = { {1,2,3},
                     {4,5,6},
                     {7,8,9}  };

    int sum = 0;
    int row = 1;
    for (int i = 0; i < 3; i++) {
        sum = sum + arr[row][i];
    }
    cout << "Sum is: " << sum << endl;
}
