#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;

int fib(int x) {
    if (x == 0)
        return 0;

    if (x == 1)
        return 1;

    return fib(x-1)+fib(x-2);
}

int main(int argc, char *argv[])
{
		int i, j, k, s;
		if (argc >= 3) {
			for (i = 0; i < argc; ++i) {
					string arg = argv[i];
					if (arg == "-l" || arg == "--limit") {
							s = atoi(argv[i+1]);
					}
			}
		}
		else {
				std::cout << "Usage: fib [-l|--limit] LIMIT" << std::endl;
				return 0;
		}
		std::cout << fib(s);

		return 0;
}
