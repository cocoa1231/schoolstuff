#include <iostream>
using namespace std;

int main(int argc, char* argv[]) {
	int array[5] = { 1, 2, 3, 4, 5 };
	int reverse[5];

	for (int i = 0; i < 5; i++) {
		reverse[i] = array[4-i];
	}

	for (int i = 0; i < 5; ++i) {
		cout << reverse[i] << endl;
	}
	return 0;
}
