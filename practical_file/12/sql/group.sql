-- Sample database `Employees` provided by dev.mysql.com
-- https://github.com/datacharmer/test_db 

-- count up number of male and female employees separately
SELECT COUNT(emp_no), gender FROM employees GROUP BY gender; 

