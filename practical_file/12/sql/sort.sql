-- Sample database `Employees` provided by dev.mysql.com
-- https://github.com/datacharmer/test_db 

-- List employees in asc order of hire_date
SELECT * from employees ORDER BY hire_date;

-- List employees in dec order of hire_date
SELECT * from employees ORDER BY hire_date desc;
