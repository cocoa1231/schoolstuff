-- Sample database `Employees` provided by dev.mysql.com
-- https://github.com/datacharmer/test_db 

-- Retrieve list of all engineers and their names
SELECT e.emp_no, t.title, CONCAT(e.first_name,  " ", e.last_name)  as 'Name'
FROM titles as t
RIGHT JOIN employees AS e ON t.emp_no = e.emp_no 
WHERE t.title IN ( 'Engineer' );
