CREATE DATABASE StudentInfo;
USE StudentInfo;

CREATE TABLE StudentInfo.Sports (
    Student  INT(2) NOT NULL, 
    Class    INT(2) NOT NULL ,
    Name     VARCHAR(30) NOT NULL ,
    GameOne  VARCHAR(30) NOT NULL , 
    GradeOne CHAR(1) NOT NULL , 
    GameTwo  VARCHAR(30) NOT NULL ,
    GradeTwo CHAR(1) NOT NULL 
);

INSERT INTO `Sports` VALUES
    ('09', '07', 'Sameer', 'Cricket', 'B', 'Swimming', 'A'),
    ('11', '8', 'Sujit', 'Tennis', 'A', 'Skating', 'B'),
    ('12', '07', 'Kamal', 'Swimming', 'B', 'Football', 'C'), 
    ('13', '07', 'Veena', 'Tennis', 'C', 'Tennis', 'A'),
    ('14', '09', 'Archana', 'Basketball', 'A', 'Cricket', 'A');

-- Display the names of the students who have grade C in either Game one or Game two or # both.
SELECT Name FROM Sports WHERE GradeOne='C' OR GradeTwo='C';

-- 2 Display the number of students getting grade A in cricket.
SELECT COUNT(*) FROM Sports WHERE GradeOne='A' AND GameTwo="Cricket" OR GradeTwo='A' AND GameTwo="Cricket";

-- 3 Display the names of the students who have same game for both Game one or Game two
SELECT Name FROM Sports WHERE GameOne=GameTwo;

--  4 Display the games taken up by the students whose name starts with A. 
SELECT GameOne, GameTwo FROM Sports WHERE Name LIKE ("A%");

-- 5 Add a new column named MARKS.
ALTER TABLE Sports
    ADD Marks FLOAT(6,2);

-- 6 Assign a value 200 for marks for all those students who are getting grade B 
-- or A in both Game one and Game two 
UPDATE Sports SET Marks=200 WHERE GradeOne<="B" AND GradeTwo<="B";

SELECT * FROM Sports ORDER BY Name;
