-- Sample database `Employees` provided by dev.mysql.com
-- https://github.com/datacharmer/test_db 

SELECT de.dept_no, e.emp_no, CONCAT(e.first_name, " ", e.last_name) as 'Name' 
FROM dept_emp as de 
LEFT JOIN employees as e ON e.emp_no = de.emp_no ORDER BY de.dept_no;
