-- Sample database `Employees` provided by dev.mysql.com
-- https://github.com/datacharmer/test_db 

SELECT d.dept_no, d.dept_name, de.emp_no, DATEDIFF(de.to_date, de.from_date) as 'Days' 
FROM dept_emp as de 
LEFT JOIN departments as d ON d.dept_no = de.dept_no  
UNION 
SELECT d.dept_no, d.dept_name, de.emp_no, DATEDIFF(de.to_date, de.from_date) as 'Days' 
FROM dept_emp as de 
RIGHT JOIN departments as d ON d.dept_no = de.dept_no;
