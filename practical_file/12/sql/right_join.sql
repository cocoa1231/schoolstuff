-- Sample database `Employees` provided by dev.mysql.com
-- https://github.com/datacharmer/test_db 

-- Get the number of days all employees have been hired for.
SELECT CONCAT(e.first_name, " ", e.last_name) AS 'Name', DATEDIFF(now(), e.hire_date) as 'Hire time' 
FROM titles as t 
RIGHT JOIN employees as e ON e.emp_no = t.emp_no;

