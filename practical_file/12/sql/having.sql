-- Sample database `Employees` provided by dev.mysql.com
-- https://github.com/datacharmer/test_db 

-- List employees having birth year above 1964
SELECT * FROM employees HAVING YEAR(birth_date) > 1964;
