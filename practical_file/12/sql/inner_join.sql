-- Sample database `Employees` provided by dev.mysql.com
-- https://github.com/datacharmer/test_db 

-- Sum salaries of employee. Primary key emp_no
SELECT COUNT(emp_no), SUM(salary) FROM salaries;

-- Show employee name and salary (inner join)
SELECT e.emp_no, CONCAT(e.first_name, " ", e.last_name) as 'Name', s.salary
FROM employees as e
INNER JOIN salaries as s ON e.emp_no = s.emp_no;
