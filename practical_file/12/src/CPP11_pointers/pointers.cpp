#include <iostream>
#include <cstring>
#include <bits/stdc++.h>
using namespace std;

struct stud{
	int rno;
	string name;
	float marks;
};

class eas{
    public:
        stud *sptr;
        void getData(){
            sptr=new stud;
            cout<<"Enter roll number: ";
            cin>>sptr->rno;

            cout<<"Enter Name: ";
            cin>>sptr->name;

            cout<<"Enter marks: ";
            cin>>sptr->marks;
        }

        void DisplayData(){
            cout<<"Roll no: "<<sptr->rno<<endl;
            cout<<"Name: "<<sptr->name<<endl;
            cout<<"Marks: "<<sptr->marks;

            delete sptr;//sprt=NULL
        }
};

int main(){
    eas a;
    a.getData();
    a.DisplayData();
    return 0;
}
