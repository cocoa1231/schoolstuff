#include <iostream>
#include <cstring>
#include <string>
#include <stdio.h>
#include <cstdlib>
using namespace std;

// To calculate volume of cylinder or cone ( choice by user), using class and constructors.

float pi=3.1415926;

class container
{
	private:
        long double radius,height,volume;
        char type[15];

	public:
		void getvalues()
		{
			cout<<"Enter radius(in cm): ";
			cin>>radius;

			cout<<endl<<"Enter height(in cm): ";
			cin>>height;

			cout<<"Enter shape:(cylinder/cone)";
			//gets(type);
            cin.ignore();
            cin.getline(type, 15);
			cout<<endl;
		}

        void calcvolume()
        {
            if(strcmp(type,"cone")==0)
                volume=(pi*radius*radius*height/3);
            else if(strcmp(type,"cylinder")==0)
                volume=(pi*radius*radius*height);
            else
                volume=0;
        }

		void showvalues();


        container()
        {
            radius=0;
            height=0;
            strcpy(type,"NA");
        }
        container(float r,float h,char t[15])
        {
            radius=r;
            height=h;
            strcpy(type,t);
        }
};


void container::showvalues()
{
    cout<<"Radius: "<<radius<<endl;
    cout<<"Height: "<<height<<endl;
    cout<<"Type: "<<type<<endl;
    cout<<"Volume: "<<volume<<" cubic cm.";
}

int main()
{
    system("clear");
    float r,h;

    char t[15],ch;
    container c1;
    do {
        c1.getvalues();
        c1.calcvolume();
        c1.showvalues();
        getchar();

        cout<<endl<<"Want to enter more?(y/n)";
        cin>>ch;
    }while(ch=='y');
}
