#include <iostream>
#include <string>
using namespace std;

class Player {
    public:
        Player(string name) {
            this->health  = 100;
            this->attack  = 0.7;
            this->defence = 0.2;
            this->name    = name;
        }

        int take_damage(double damage) { 
            this->health -= damage;
            if (this->health <= 0) {
                cout << "Player dead" << endl;
            }
            return this->health;   
        }

        ~Player() {
            cout << "Goodbye" << endl;
        }
    private:
        double health, attack, defence;
        string name;
};

int main()
{
    Player Jake("Jake");
    Jake.take_damage(100.0);
    return 0;
}
