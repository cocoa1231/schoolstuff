#include <iostream>
#include <string>
using namespace std;

void add(int a, int b) {
    cout << a + b << endl;
}

void add(double a, double b) {
    cout << a + b << endl;
}

void add(string a, string b) {
    cout << a + b << endl;
}


int main() {
    add(1, 2);
    add(1.0, 2.0);
    add((string) "hello", (string) " world");
}
