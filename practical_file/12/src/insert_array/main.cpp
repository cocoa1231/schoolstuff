#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
    int array[] = {1, 2, 3, 4};
    ssize_t size = (ssize_t) sizeof(array) / sizeof(array[0]);
    cout << size << endl << endl;
    int input, index;
    cout << "Enter value: ";
    cin  >> input;
    cout << "Enter index: ";
    cin  >> index;

    if (index < 0 || index > size)
        return -1;

    int final_array[size+1];
    for (int i = 0; i <= size; ++i) {
        if (i == index) 
            final_array[i] = input;
        else if (i > index) 
            final_array[i+1] = array[i];
        else
            final_array[i] = array[i];
    }

    for (int i = 0; i <= size; ++i) {
        cout << final_array[i] << " ";
    }
    cout << endl;
    return 0;
}
