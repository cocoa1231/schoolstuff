#include <iostream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

class dance
{
    private:
        int rno;
        char name[50];
        char style[50];
        float fee;

        void checkfee()
        {
            if(strcmp(style,"classical")==0)
                fee=10000;
            else if(strcmp(style,"western")==0)
                fee=8000;
            else if(strcmp(style,"freestyle")==0)
                fee=11000;
        }

    public:
        void enrol()
        {
            cout << "Enter roll number: ";
            cin  >> rno;

            cout << endl << "Enter name: ";
            cin.ignore();
            cin.getline(name, 50);

            cout << endl << "Enter style: ";
            cin.ignore();
            cin.getline(style, 50);
            cout << endl;

            checkfee();
        }

        void showdata() {
           cout << "Roll no." << rno   << endl
                << "Name: "   << name  << endl
                << "Style: "  << style << endl
                << "Fee: "    << fee;
        }
};

int main()
{    
    system("clear");
	int  ns;
    dance a[50];
    cout<<"Enter number of  students: ";
    cin>>ns;

	for (int i = 0; i<ns; i++) {
	   cout<<"Enter details of student "<<i+1<<endl;
	   a[i].enrol();
	}

	for (int i = 0; i < ns; i++) {
        cout<<endl<<"Details of student "<<i+1<<":"<<endl;
        a[i].showdata();
	}
    getchar();
}
