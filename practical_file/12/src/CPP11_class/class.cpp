#include <iostream>
#include <string.h>
#include <iomanip>
#include <cstdlib>
using namespace std;

class stud
{
    private:
        int marks[5];
        int rno;
        char name[20];

    public:

        void getdata();
        void displaydata();

};

void stud::getdata()
{
    cout<<"____________________________________"<<endl;
    cout<<"Enter student roll number : ";
    cin>>rno;
    cout<<endl<<"Enter student name: ";
    cin>>name;
    cout<<endl;

    for(int i=0;i<5;i++) {
        cout<<"Enter marks of subject "<<i+1<<" : ";
        cin>>marks[i];
        cout<<endl;
    }
}



void stud::displaydata()
{
    cout<<"Roll Number: "<<rno<<"  ";
    cout<<"Name: "<<name<<endl<<endl;
    cout<<"_________________________________________"<<endl;

    for (int j=0;j<5;j++)
        cout<<"Subject "<<j+1<<"||";

    cout<<endl<<endl;

    for (int j=0; j < 5; j++)
        cout<<setw(8)<<marks[j]<<"  ";

    cout<<endl<<endl;
}

int main()
{
    system("clear");
    int ns;

    stud s[20];
    cout << "Enter number of students (at most 20): ";
    cin  >> ns;
    cout << endl;

    for(int i=0;i<ns;i++)
    {
        cout<<"Enter details of student "<<i+1<<endl<<endl;
        s[i].getdata();
        cout<<endl<<"___________________________________________"<<endl<<endl<<endl;
    }

    for(int i=0;i<ns;i++)
    {
        cout<<"_______________________________"<<endl;
        cout<<"Details of student "<<i+1<<" : "<<endl;
        cout<<"_______________________________"<<endl;
        s[i].displaydata();
        cout<<endl<<endl<<endl;
    }


    getchar();
    return 0;
}
