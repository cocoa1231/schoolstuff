#include <iostream>
using namespace std;


class Weapon {
    public:
        Weapon() {
            cout << "Weapon created" << endl;
        }
        virtual void attack(int id){}
        virtual void uhealth(double amount){}
};

class Sword : public Weapon {
    public:
        Sword() {
            cout << "Sword" << endl;
        }
        void attack(int id) {
            cout << "I attacked " << id << endl;
        }
        void uhealth(double amount) {
            cout << "Took damage: " << amount;
        }
};

int main() {
    Sword s1;
    s1.attack(2);
    s1.uhealth(23.02);
}
