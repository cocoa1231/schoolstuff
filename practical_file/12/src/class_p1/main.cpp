#include <iostream>
#include <string>

class Data {
public:
    int rno;
    std::string name;
    double marks;

    void display() {
        std::cout << "Roll Number: " << this->rno << std::endl
                  << "Name: " << this->name << std::endl
                  << "Marks: " << this->marks << std::endl;

    }
};

int main(int argc, char *argv[])
{
    Data d1;
    d1.rno = 1;
    d1.name = (std::string) "cocoa";
    d1.marks = 0;
    d1.display();
    return 0;
}
