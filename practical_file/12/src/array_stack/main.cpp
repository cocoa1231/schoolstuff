#include <iostream>
#define STACK_SIZE 30

class Stack {
    public:
        void push(int item) {
            this->stk[this->top_index+1] = item;
            this->top_index++;
        }

        int pop() {
            if (this->top_index < 0) {
                std::cout << "Underflow Error: Empty stack" << std::endl;
            }
            int ret = this->stk[this->top_index];
            this->top_index--;

            return ret;
        }

        void dump() {
            int i;
            for (i = 0; i < this->top_index+1; ++i) {
                std::cout << this->stk[i] << std::endl;
            }
        }
    private:
        int stk[STACK_SIZE];
        int top_index = -1;
};

int main(int argc, char *argv[])
{
    Stack s;

    s.push(1);
    s.push(2);
    s.push(3);

    s.dump();

    s.pop();
    s.dump();
    return 0;
}
