#include <iostream>
#define MAX_Q_SIZE 30

class Queue {
    public:
        
        Queue(int opcode) {
            this->queue[this->top_index+1] = opcode;
            this->top_index++;
            this->bottom_index++;
        }

        void enque(int opcode) {
            this->queue[this->top_index+1] = opcode;
            this->top_index++;
        }

        int deque() {
            if (bottom_index > top_index) 
                std::cout << "Error: Underflow. Empty queue" << std::endl;

            int ret = this->queue[this->bottom_index];
            
            this->bottom_index++;

            return ret;
        }

        void printq() {
            int i;
            for (i = bottom_index; i <= top_index; ++i) {
                std::cout << this->queue[i] << std::endl;
            }
        }


    private:
        int queue[MAX_Q_SIZE];
        int top_index = -1;
        int bottom_index = -1;
};

int main(int argc, char *argv[])
{
    Queue Q(1);

    Q.enque(2);
    Q.enque(3);
    Q.enque(5);

    std::cout << "Displaying: " << std::endl;
    Q.printq();

    Q.deque();
    Q.deque();

    std::cout << "Displaying: " << std::endl;
    Q.printq();

    Q.deque();
    Q.deque();
    Q.deque();

    return 0;
}

