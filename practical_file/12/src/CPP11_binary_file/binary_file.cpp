#include <iostream>
#include <cstring>
#include <fstream>
#include <bits/stdc++.h>
#include <stdio.h>
using namespace std;

class stock {
    public:
        int roll_no;
        char name[30];
        char course[30];
        int admission_no;


        void input()
        {
            cout<<"Enter roll  number: ";
            cin>>roll_no; cin.ignore();

            cout<<"Enter name: ";
            cin.getline(name, 30); 

            cout<<"Enter course: ";
            cin.getline(course, 30); 

            cout<<"Enter admission number: ";
            cin>>admission_no; cin.ignore();

        }

        void output()
        {
            cout<<"Roll no: "<<roll_no<<endl<<"Name: "<<name<<endl<<"Course: "<<endl;
            cout<<"Admission number: "<<admission_no;
        }

};

void create(string Fname)
{
	ofstream fil;
	stock S;
	fil.open(Fname.c_str(), ios::binary | ios::out);
	char choice;

	do {
		S.input();
		fil.write((char*)&S,sizeof(S));
		cout<<"Want to enter more records?(y/n): ";
		cin>>choice;

	} while(choice=='y' || choice=='Y');
	fil.close();
}

void display(string Fname)
{
    cin.ignore();
	fstream fil; stock s;
	fil.open(Fname.c_str(), ios::binary | ios::in);
	fil.read((char*)&s, sizeof(s));
	while(!fil.eof())
	{
        cout << "Name: " << s.name << endl
             << "Roll Number: " << s.roll_no << endl
             << "Course: " << s.course << endl
             << "Admission No.: " << s.admission_no << endl;
		fil.read((char*)&s, sizeof(s));
	}
}

void search(string Fname)
{
	fstream fil; stock s;
	int InoS,found=0;

	fil.open(Fname, ios::binary| ios::in);
	cout<<"Enter Record to be searched: ";
	cin>>InoS; cin.ignore();
	cout<<endl;

	while(fil.read((char*)&s,sizeof(s)))
	{
		if(s.roll_no==InoS)
		{
			s.output();
			found++;
		}
	}
	if(found==0)
	{
		cout<<"Record not found."<<endl;
	}
	fil.close();
}

int count_rec(string Fname)
{
	fstream fil; stock s;
	int count=0;

	fil.open(Fname.c_str(), ios::binary|ios::in);

	while(fil.read((char*)&s, sizeof(s)))
      count++;

    cout<<"No. of records: "<<count<<endl;
	fil.close();
	return count;
}

void delete_record(string fn1, string fn2)
{
	fstream f1,f2;
	stock s; int InoD;

    f1.open(fn1.c_str(), ios::binary|ios::in);
    f2.open(fn2.c_str(), ios::binary|ios::in);

    cout<<"Enter record no. to be deleted: ";
    cin>>InoD;
    while(f1.read((char*)&s,sizeof(s)))
	{
		if(s.roll_no!=InoD)
		{
			f2.write((char*)&s, sizeof(s));
		}
	}
    f1.close();
    f2.close();

    remove(fn1.c_str());
    rename(fn2.c_str() , fn1.c_str());
    cout<<"Record deleted successfully.";
}

void transfer_file(string Fname){
    ifstream fil1;
    ofstream fil2;
    string new_file_name;
    stock s;
    fil1.open(Fname, ios::binary| ios::in);
    cout<<"Enter new file name: ";
    cin>>new_file_name;
    fil2.open(new_file_name, ios::binary| ios::out);
    while(fil1.read((char*)&s,sizeof(s)))
	{
		fil2.write((char*)&s, sizeof(s));
	}
    fil1.close();
    fil2.close();

}

int main(){
    string Fname;
    int ch;
    cout<<"Enter file name: ";
    cin>>Fname;
    do{cout<<endl<<endl<<"Manipulating "<<Fname<<endl<<endl;
        cout<<"1.Create Records"<<endl<<"2.Display Records"<<endl;
        cout<<"3.Search for a record."<<endl<<"4.Count the number of records."<<endl;
        cout<<"5.Transfer records of this file to another."<<endl;
        cout<<"6.Delete a record."<<endl<<"7. Exit" << "\n>>> ";
        cin.ignore();
        cin >> ch;
        switch(ch){
             case 1: create(Fname); break;
             case 2: display(Fname); break;
             case 3: search(Fname); break;
             case 4: count_rec(Fname); break;
             case 5: transfer_file(Fname); break;
             case 6: delete_record(Fname,"File2.dat"); break;
         }
    }while(ch!=7);
    return 0;
}
  

 
