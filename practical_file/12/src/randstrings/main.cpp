#include <iostream>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[])
{
    char* array_of_strings[5] = { "first", "second", "third", "fourth", "fifth" };
    srand(time(NULL));

    int rint = rand() % 5, i = 0;

    while(array_of_strings[rint][i] != '\0') {
        std::cout << array_of_strings[rint][i];
        i++;
    }

    return 0;
}
