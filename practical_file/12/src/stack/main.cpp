#include <iostream>
#include <string>

struct node {
    node *next;
    std::string item;

    void set(std::string _item) {
        item = _item;
    }
};


class Stack {
public:

    Stack(std::string first_item) {
        node *temp = new node;

        temp->set(first_item);
        temp->next = NULL;

        this->top = temp;
    }

    void push(std::string newItem) {
        node *temp = new node;

        temp->set(newItem);
        temp->next = top;
        this->top  = temp;
    }

    void display() {
        node *cnode = this->top;
        while ( cnode != NULL ) {
            std::cout << cnode->item << std::endl;
            cnode = cnode->next;
        }
    }

    void pop_top() {
        if (this->top == NULL) {
            std::cout << "Underflow error: Stack empty. Push. dammit" << std::endl;
            return;
        }

        node *first = this->top;

        this->top = top->next;

        delete first;
    }

    bool isEmpty() {
        if (this->top == NULL) {
            return true;
        }
        return false;
    }


private:
    node *top;
};

int main(int argc, char *argv[])
{
    std::string temp = "first";
    Stack test (temp);

    //test.display();
    temp = "second";
    test.push(temp);

    //test.display();
    temp = "third";
    test.push(temp);

    test.display();
    test.pop_top();
    //test.display_top();
    test.display();

    while(!test.isEmpty()) {
        test.pop_top();
        std::cout << "____________" << std::endl;
        test.display();
    }

    test.pop_top();

    return 0;
}
