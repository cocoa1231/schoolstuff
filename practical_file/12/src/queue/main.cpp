#include <iostream>

struct node {
    struct node* next;
    int opcode;
};

class Queue
{
private:
    struct node* bottom;
    struct node* top;
public:
    Queue(int init_opcode) {
        struct node* temp = new node;
        temp->opcode = init_opcode;
        temp->next   = NULL;

        this->bottom = temp;
        this->top = temp;
    }
    
    void enqueue(int new_opcode) {
        struct node* temp = new node;
        temp->opcode = new_opcode;
        temp->next = NULL;

        this->top->next = temp;
        this->top = temp;
    }

    int dequeue() {
        if (this->bottom == NULL) {
            std::cout << "Underflow Error" << std::endl;
            return -1;
        }
        struct node* temp = new node;
        int rval = this->bottom->opcode;
        temp = this->bottom;

        this->bottom = this->bottom->next;
        delete temp;

        return rval;
    }

    void printq() {
        struct node* temp = new node;
        temp = this->bottom;
        while( temp != NULL ) {
            std::cout << "Opcode: " << temp->opcode << std::endl;
            temp = temp->next;
        }
    }

};


int main(int argc, char *argv[])
{
    Queue Q (10);

    Q.enqueue(11);
    Q.enqueue(12);

    Q.printq();

    Q.dequeue();
    Q.dequeue();
    Q.printq();

    Q.dequeue();
    Q.dequeue();


    
    return 0;
}
