#include <iostream>

int main(int argc, char *argv[])
{
    int array1[] { 1, 2, 3 };
    int size1 = sizeof(array1)/sizeof(array1[0]);
    int array2[] { 4, 5, 6, 7 };
    int size2 = sizeof(array2)/sizeof(array2[0]);
    int total_size = size1 + size2, i;
    int final_array[ total_size ];

    for (i = 0; i < size1; ++i) {
        final_array[i] = array1[i];
    }

    for (i = size1; i < total_size; ++i) {
        final_array[i] = array2[i-size1];
    }

    for (i = 0; i < total_size; ++i) {
        std::cout << final_array[i] << " "; 
    }

    std::cout << std::endl;


    return 0;
}
