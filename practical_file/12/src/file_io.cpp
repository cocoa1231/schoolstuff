#include <fstream>
#include <iostream>
#define FNAME_SIZE 50
using namespace std;

// Prototypes
int create_file(std::string);
int append_file(std::string);
int insert_file(std::string);
int delete_lines(std::string);
int delete_file(std::string);

int main_menu() {
    std::string filename;
    bool quit = false;
    while (!quit) {
        std::cout << R"(
     __  __       _         __  __                  
    |  \/  | __ _(_)_ __   |  \/  | ___ _ __  _   _ 
    | |\/| |/ _` | | '_ \  | |\/| |/ _ \ '_ \| | | |
    | |  | | (_| | | | | | | |  | |  __/ | | | |_| |
    |_|  |_|\__,_|_|_| |_| |_|  |_|\___|_| |_|\__,_|

        )" << std::endl; // Got the art from figlet
        std::cout << "What do you wanna do?" << std::endl;
        std::cout << "1. Create a file" << std::endl;
        std::cout << "2. Append to a file" << std::endl;
        std::cout << "3. Insert to a line number" << std::endl;
        std::cout << "4. Delete selected lines" << std::endl;
        std::cout << "5. Delete a file" << std::endl;
        std::cout << "6. Exit" << std::endl;
        int choice;
        std::cout << "file_io> ";
        std::cin >> choice;
        switch (choice) {
            case 1:
                std::cin.ignore();
                std::cout << "Enter exact filename: ";
                std::cin.clear();
                fflush( stdin );
                std::cin >> filename;
                create_file(filename);
                break;
            case 2:
                std::cout << "Enter exact filename: ";
                std::cin.clear();
                fflush( stdin );
                std::cin >> filename;
                append_file(filename);
                break;
            case 3:
                std::cout << "Enter exact filename: ";
                std::cin.clear();
                fflush( stdin );
                std::cin >> filename;
                insert_file(filename);
                break;
            case 4:
                std::cout << "Enter exact filename: ";
                std::cin.clear();
                std::cin >> filename;
                delete_lines(filename);
                break;
            case 5:
                std::cout << "Enter exact filename: ";
                std::cin.clear();
                fflush( stdin );
                std::cin >> filename;
                delete_file(filename);
                break;
            case 6:
                return 0;
            default:
                std::cout << "Could not figure out what that was." << std::endl;
        }
    }
    return 0;
}

int create_file(std::string filename) {
    std::ofstream ofile (filename.c_str());
    ofile.close();
    return 0;
}

int append_file(std::string filename) {
    std::ofstream ofile (filename.c_str(), std::ios::app);
    std::cout << "How many lines: " << std::endl;
    double nlines; int i;
    for (i = 0; i < nlines; ++i) {
        std::string line;
        std::cin.ignore();
        std::cin >> line;
        ofile << line;
    }
    return 0;
}

int main(int argc, char *argv[])
{
    main_menu(); 
    return 0;
}
