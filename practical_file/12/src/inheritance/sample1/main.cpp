#include <iostream>
#include <string>
#include <vector>

class TopLevel {
    public:
        int publicint = 0;
    private:
        int privateint = 1;

    protected:
        int protectedint = 2;
};

class InClass : protected TopLevel {
    public:
        InClass() {
            std::cout << publicint << protectedint << std::endl;
        }
};

int main()
{
    InClass cls;
    return 0;
}
