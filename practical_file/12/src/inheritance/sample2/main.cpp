#include <iostream>
#include <string>
#include <vector>

using namespace std;

class TopLevel {
    public:
        TopLevel() {
            cout << "Class Created" << endl;
        }
        ~TopLevel() {
            cout << "Class destroyed" << endl;
        }
        std::string publicstring = "public";
    private:
        std::string privstring   = "private";
    protected:
        std::string protstring   = "protected";
};

class PubInClass : public TopLevel {
    public:
        PubInClass() {
            cout << "Inheritance mode: Public" << endl;
        }
        void printAvailDataMembers() {
            cout << "Public String: " << publicstring \
                      << "Private string: " << protstring   << endl;
        }
};

class ProtInClass : protected TopLevel {
    public:
        ProtInClass() {
            cout << "Inheritance Mode: Protected" << endl;
        }
        void printAvailDataMembers() {
            cout << "Public String: " << publicstring \
                      << "Protected String: " << protstring << endl;
        }
};

class PrivInClass : private TopLevel {
    public:
        PrivInClass() {
            cout << "Inheritance Mode : Private" << endl; 
        }
        void printAvailDataMembers() {
            cout << "Public String: " << publicstring \
                 << "Protected string: " << protstring << endl; 
        }
};

int main() {
    TopLevel TLC;
    PubInClass Pub;
    PrivInClass Priv;
    ProtInClass Prot;

    Pub.printAvailDataMembers();
    Prot.printAvailDataMembers();
    Priv.printAvailDataMembers();


}
