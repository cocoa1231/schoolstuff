#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
//#include<conio.h>
using namespace std;

int bsearch(int a[],int e,int size)
{
	int l=0, u=size-1, mid;

	while(l<=u) {
        mid=(l+u)/2;
		if(a[mid]==e)
        	return(mid+1);
	    else if(a[mid]>e)
	    	u=mid-1;
	    else 
	    	l=mid+1;
	}
    return -1;
}

int main()
{
    int arr[10];
    int element;
    int check=0;
    for (int i = 0; i < 10; i++) {
        cout << "Enter element " << i << ": "; 
    	cin  >> arr[i];
    }

    cout << "Sorting as ascending..." << endl;
    sort(arr, arr+10);

    cout<<"Enter elemevnt to be searched: ";
    do {
        cin>>element;

        int x=bsearch(arr,element,10);

        if(x==-1)
            cout<<"Element not found. Please enter again.";
        else {
            cout<<"Element found at position "<<x<<endl<<"Press any key to continue.";
            check=1;
            getchar();
        }

    } while(check==0);
 }
