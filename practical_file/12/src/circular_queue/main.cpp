#include <iostream>
#include <stdexcept>

struct node {
    struct node* next;
    int opcode;
};

class Queue
{
private:
    struct node* bottom;
    struct node* top;
    int queuelen = 0, maxlen;

public:
    Queue(int init_opcode, int bufferlen = -1) {
        if (bufferlen == -1) {
            throw "Invalid/Missing argument: Queue len > 0";
        } 
        struct node* temp = new node;
        temp->opcode = init_opcode;
        temp->next   = NULL;

        this->bottom = temp;
        this->top = temp;
        
        this->queuelen++;
        this->maxlen = bufferlen;
    }
    
    void enqueue(int new_opcode) {
        if (this->queuelen >= this->maxlen) {
            std::cout << "Cannot append to full queue" << std::endl;
            return;
        }
        struct node* temp = new node;
        temp->opcode = new_opcode;
        temp->next = NULL;

        this->top->next = temp;
        this->top = temp;
        this->queuelen++;

        std::cout << "New length: " << this->queuelen << std::endl;

    }

    int dequeue() {
        if (this->bottom == NULL) {
            std::cout << "Underflow Error" << std::endl;
            return -1;
        }
        struct node* temp = new node;
        int rval = this->bottom->opcode;
        temp = this->bottom;

        this->bottom = this->bottom->next;
        delete temp;
            
        this->queuelen--;
        return rval;
    }

    void printq() {
        struct node* temp = new node;
        temp = this->bottom;
        while( temp != NULL ) {
            std::cout << "Opcode: " << temp->opcode << std::endl;
            temp = temp->next;
        }
    }

};


int main()
{
    Queue Q (10, 5);

    Q.enqueue(11);
    Q.enqueue(12);
    Q.enqueue(13);
    Q.enqueue(14);
    Q.enqueue(15);

    Q.printq();

    Q.enqueue(16);

    Q.dequeue();
    Q.dequeue();
    Q.printq();

    Q.dequeue();
    Q.dequeue();
    Q.dequeue();
    Q.dequeue();
    Q.dequeue();

    
    return 0;
}
