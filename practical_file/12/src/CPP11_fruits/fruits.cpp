#include <iostream>
#include <stdlib.h>
#include <stdio.h>
using namespace std;

int main()
{
    char fruits[10][50];
   
    for (int i = 0; i < 10; i++) {
        cout<<"Enter fruit number "<<i+1<<" : ";
        cin.ignore();
        cin.getline(fruits[i], 50);
    }

    int x=(rand()%10)+1;

    cout<<"Random fruit is "<<fruits[x];
    getchar();
    return 0;
}
