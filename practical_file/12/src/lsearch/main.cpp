#include <iostream>
#include <algorithm>

int main(int argc, char *argv[])
{
    int array[] { 1, 2, 3, 4, 5 };
    std::reverse(array, array + 5); //  Reverse the array for fun
    int target, i;
    std::cout << "Target integer: ";
    std::cin >> target;
    double len_array = sizeof(array)/sizeof(array[0]);

    for (i = 0; i < len_array; ++i) {
        if (array[i] == target) {
            std::cout << "Found element at " << i+1 << std::endl;
            return 0;
        }
    }
    
    std::cout << "Error: Could not find element" << std::endl;
    return -1;
}
