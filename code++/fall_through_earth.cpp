#include <iostream>
#include <string>
#include <vector>
#include <cmath>

// Constants
#define M_earth 5.972e+24
#define rho 5515
#define G 6.67408e-11
#define R_earth 6.371e6
#define PI 3.141592

double accel(double radius, bool flag) {
    if(radius != 0) {
        if(flag) {
            return (G* (4*rho*PI*pow(radius, 3))/3 )/pow(radius, 2); 
        }
        else {
            return -1*(G* (4*rho*PI*pow(radius, 3)/3) / pow(radius, 2));
        }
    }
    else {
        return 0;
    }
}

double f(double dist, double &vel, double acceleration) {
    /* @breif: f(dist, velocity, acceleration) -> (new velocity, time)
     * %param dist: double distance to travel
     * %param vel: initial velocity
     * %param acceleration: acceleration during the travel
     */
    
    double v_prime = sqrt(pow(vel,2) + 2*acceleration*dist);
    double time =  (v_prime - vel) / acceleration;
    vel = v_prime; // Change initial velocity to final velocity
    return time; 
}

int main(int argc, char *argv[])
{
    double velocity = 0;
    double acceleration = 0;
    double total_time = 0;
    
    double ds = R_earth/10000; 

    // integrate
    for (int i = 0; i < 10000; ++i) {
        total_time += f(ds, velocity, accel(R_earth-(i*ds), true));
    }
    
    std::cout << "Halfway time: " << total_time  << std::endl;

    for (int i = 0; i < 10000; ++i) {
        total_time += f(ds, velocity, accel(R_earth-(i*ds), false));
        //std::cout << velocity << ", " << total_time << std::endl;
    }


    std::cout << "Total time: "<< total_time << " seconds"<< std::endl;

    return 0;
}
