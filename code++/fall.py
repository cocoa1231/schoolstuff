#!/usr/bin/env python

from math import *
from sys import argv
import matplotlib.pyplot as plt
import numpy as np


M_EARTH = 5.972e+24
rho     = 5515
G       = 6.67408e-11
R_EARTH = 6.371e6


def get_arg(flag):
    return argv[argv.index(flag)+1]

def accel(radius, flag):
    if radius != 0:
        if flag:
            return (G * 4/3 * pi * radius**3 * rho)/radius**2
        elif not flag:
            return -1*(G * 4/3 * pi * radius**3 * rho)/radius**2
    else:
        return 0

def time(distance, initial_vel, acceleration):

    new_vel = sqrt(initial_vel**2 + 2*acceleration*distance)
    time    = (new_vel - initial_vel) / acceleration

    return (new_vel, time)

if __name__ == "__main__":
    vel   = 0
    acc   = 0
    ttime = 0
    denom = int(get_arg('-ds'))
    ds    = R_EARTH/denom

    x_values = []
    y_values = []

    # Integrate
    for i in range(denom):
        # r = time(ds, vel, accel(R_EARTH-(i*ds), True))
        # print(r)
        # vel   += r[0]
        # ttime += r[1]
        x_values.append(i)
        y_values.append(accel(R_EARTH-(i*ds), True))

    plt.grid() 
    plt.plot(x_values, y_values)
    plt.savefig('acceleration_plot.png')
    plt.show()

    print("Halfway time", ttime)
